## Steps for Installation

- [Laravel Installation](https://laravel.com/docs/9.x/installation#your-first-laravel-project)
```
laravel new laravel9_breeze_vue
```

- [Git initiation](https://docs.gitlab.com/ee/tutorials/make_your_first_git_commit.html#create-a-sample-project)
```
git init
git remote add origin https://gitlab.com/yousaf.bsse1735/laravel9_breeze_vue.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

- [Create DB](https://hevodata.com/learn/xampp-mysql)

- [Edit host file](https://www.howtogeek.com/27350/beginner-geek-how-to-edit-your-hosts-file/)
in folder C:\Windows\System32\drivers\etc
open file hosts
and add following
```
127.0.0.1 laravel9_breeze_vue.test
```

- [configure virtual host](https://www.cloudways.com/blog/configure-virtual-host-on-windows-10-for-wordpress/)
in folder C:\xampp\apache\conf\extra
open file httpd-vhosts.conf
and add following
```
<VirtualHost *:80>
DocumentRoot "c:/xampp/htdocs/laravel9_breeze_vue/public"
ServerName laravel9_breeze_vue.test
<Directory "c:/xampp/htdocs/laravel9_breeze_vue/public">
</Directory>
</VirtualHost>
```
## open browser and Run

```
http://laravel9_breeze_vue.test
```

## Install breeze with Inertia (Vue)

- [Install breeze with Inertia (Vue)](https://laravel.com/docs/9.x/starter-kits#breeze-and-inertia)

```
composer require laravel/breeze --dev

php artisan breeze:install vue

php artisan migrate
npm install
npm run dev
```

### Login with google

- [Create Project](https://console.cloud.google.com)

Add Credentials OAuth Client ID

Authorised JavaScript origins
http://laravelvue.com

Add Authorised redirect URIs
http://laravelvue.com/auth/google/callback

Fill OAuth Consent Screen
User Type External
Add Test Users

- [Add socialite package](https://laravel.com/docs/9.x/socialite)

